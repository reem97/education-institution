<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login')->name('login');



Route::middleware('auth:api')->group(function () {
    Route::apiResource('teachers', 'API\TeacherController');
    Route::post('user/teacher/{id}', 'API\TeacherController@addTeacherInformation');
    Route::get('subject/teachers/{id}', 'API\TeacherController@TeachersForSubject');
});

Route::middleware('auth:api')->group(function () {
    Route::apiResource('users', 'API\UserController');
});

Route::middleware('auth:api')->group(function () {
    Route::apiResource('students', 'API\StudentController');
    Route::post('user/student/{id}', 'API\StudentController@addStudentInformation');
    Route::get('year/students/{id}', 'API\StudentController@StudentsForYear');
});

Route::middleware('auth:api')->group(function () {
    Route::apiResource('subjects', 'API\SubjectController');
    Route::get('year/subjects/{id}', 'API\SubjectController@SubjectsForYear');
});

Route::middleware('auth:api')->group(function () {
    Route::apiResource('years', 'API\AcademicYearController');
});
