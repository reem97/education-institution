<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'super_admin' => [
            'users' => 'c,r,u,d,s',
            'teachers' => 'c,r,u,s',
            'students' => 'c,r,u,s',
            'subjects' => 'c,r,u,d,s',
            'years' => 'r,s',
        ],
        'teacher' => [
            'users' => 'r,s',
            'teachers' => 'r,s',
            'students' => 'r,s',
            'subjects' => 'c,r,u,d,s',
            'years' => 'r,s',
        ],
        'student' => [
            'users' => 'r,s',
            'teachers' => 'r,s',
            'students' => 'r,s',
            'subjects' => 'r,s',
            'years' => 'r,s',
        ],
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
        's' => 'show'
    ]
];
