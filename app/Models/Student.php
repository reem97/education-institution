<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'academic_id',
        'birthday',
        'phone'
    ];


    /**
     * Undocumented function
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function academic_year()
    {
        return $this->belongsTo(AcademicYear::class, 'academic_id');
    }
}
