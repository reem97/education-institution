<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicYear extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];


    /**
     * Undocumented function
     *
     * @return void
     */
    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }
}
