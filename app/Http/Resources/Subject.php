<?php

namespace App\Http\Resources;

use App\Http\Resources\AcademicYear as AcademicYearResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\AcademicYear;

class Subject extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'academic_year' => new AcademicYearResource(AcademicYear::findOrFail($this->academic_id)),
            'name' => $this->name,
            'created_at' => $this->created_at->format('d/m/y'),
            'updated_at' => $this->updated_at->format('d/m/y'),
        ];
    }
}
