<?php

namespace App\Http\Controllers\API;

use App\Models\AcademicYear;
use App\Models\Subject;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\AcademicYear as AcademicYearResource;
use App\Http\Requests\Api\years\IndexYearsRequest;
use App\Http\Requests\Api\years\ShowYearsRequest;

class AcademicYearController extends BaseController
{
    /**
     * Undocumented function
     *
     * @param IndexYearsRequest $request
     * @return void
     */
    public function index(IndexYearsRequest $request)
    {
        if ($request->authorize() == true) {
            $years = AcademicYear::all();
            return $this->sendResponse(AcademicYearResource::collection($years), 'all years sent successfully');
        }
    }


    public function show(ShowYearsRequest $request, int $id)
    {
        if ($request->authorize() == true) {
            $year = AcademicYear::findOrFail($id);
            return $this->sendResponse(new AcademicYearResource($year), 'year founded successfully');
        }
    }
}
