<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\Api\Users\IndexUserRequest;
use App\Http\Requests\Api\Users\StoreUserRequest;
use App\Http\Requests\Api\Users\ShowUserRequest;
use App\Http\Requests\Api\Users\UpdateUserRequest;
use App\Http\Requests\Api\Users\DestroyUserRequest;

class UserController extends BaseController
{

    /**
     * Undocumented function
     *
     * @param IndexUserRequest $request
     * @return void
     */
    public function index(IndexUserRequest $request)
    {
        if ($request->authorize() == true) {
            $users = User::all();
            return $this->sendResponse(UserResource::collection($users), 'all users sent successfully');
        }
    }

    /**
     * Undocumented function
     *
     * @param StoreUserRequest $request
     * @return void
     */
    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->setPassword($user->password);
        $user->save();
        $user->attachRoles($request->role_id);
        return $this->sendResponse(new UserResource($user), 'user added successfully');
    }


    /**
     * Undocumented function
     *
     * @param ShowUserRequest $request
     * @param integer $id
     * @return void
     */
    public function show(ShowUserRequest $request, int $id)
    {
        if ($request->authorize() == true) {
            $user = User::findOrFail($id);
            return $this->sendResponse(new UserResource($user), 'user founded successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param UpdateUserRequest $request
     * @param integer $id
     * @return void
     */
    public function update(UpdateUserRequest $request, int $id)
    {
        $user = User::findOrFail($id);
        $input = $request->all();
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->password = $input['password'];
        $user->setPassword($user->password);
        $role_id = $request->input('role_id');
        $role = Role::find($role_id);
        $user->syncRoles($role);
        $user->save();

        return $this->sendResponse(new UserResource($user), 'user updated successfully');
    }


    /**
     * Undocumented function
     *
     * @param DestroyUserRequest $request
     * @param integer $id
     * @return void
     */
    public function destroy(DestroyUserRequest $request, int $id)
    {
        if ($request->authorize() == true) {
            $user = User::findOrFail($id);
            $user->detachRoles($user->roles);
            $user->delete();
            return $this->sendResponse(new UserResource($user), 'user deleted successfully');
        }
    }
}
