<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{

    /**
     * Undocumented function
     *
     * @param [type] $result
     * @param String $message
     * @return void
     */
    public function sendResponse($result, String $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message
        ];
        return response()->json($response, 200);
    }


    /**
     * Undocumented function
     *
     * @param String $error
     * @param array $errorMessage
     * @param integer $code
     * @return void
     */
    public function sendError(String $error, array $errorMessage = [], int $code = 404)
    {
        $response = [
            'success' => false,
            'data' => $error
        ];
        if (!empty($errorMessage)) {
            $response['data'] = $errorMessage;
        }
        return response()->json($response, $code);
    }
}
