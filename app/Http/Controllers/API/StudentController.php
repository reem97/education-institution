<?php

namespace App\Http\Controllers\API;

use App\Models\Student;
use App\Models\User;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Student as StudentResource;
use App\Http\Requests\Api\Students\StudentRequestIndex;
use App\Http\Requests\Api\Students\StudentRequestShow;
use App\Http\Requests\Api\Students\StudentRequestAdd;
use App\Http\Requests\Api\Students\StudentRequestUpdate;

class StudentController extends BaseController
{

    /**
     * Undocumented function
     *
     * @param StudentRequestAdd $request
     * @param integer $id
     * @return void
     */
    public function addStudentInformation(StudentRequestAdd $request, int $id)
    {
        $user = User::where('id', $id)->whereHas('roles', function ($q) {
            $q->where('id', 3);
        })->first();

        if (is_null($user)) {
            return $this->sendError('student not found');
        } else {
            $student = Student::create([
                'user_id' => $id,
                'academic_id' => $request->academic_id,
                'birthday' => $request->birthday,
                'phone' => $request->phone
            ]);
        }

        return $this->sendResponse(new StudentResource($student), 'Student’s information sent successfully');
    }


    /**
     * Undocumented function
     *
     * @param StudentRequestIndex $request
     * @return void
     */
    public function index(StudentRequestIndex $request)
    {
        if ($request->authorize() == true) {
            $students = Student::all();
            return $this->sendResponse(StudentResource::collection($students), 'all students sent successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param StudentRequestShow $request
     * @param integer $id
     * @return void
     */
    public function show(StudentRequestShow $request, int $id)
    {
        if ($request->authorize() == true) {
            $student = Student::findOrFail($id);
            return $this->sendResponse(new StudentResource($student), 'student founded successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param StudentRequestUpdate $request
     * @param integer $id
     * @return void
     */
    public function update(StudentRequestUpdate $request, int $id)
    {
        $student = Student::findOrFail($id);
        $input = $request->all();
        $student->academic_id = $input['academic_id'];
        $student->birthday = $input['birthday'];
        $student->phone = $input['phone'];
        $student->save();

        return $this->sendResponse(new StudentResource($student), 'student updated successfully');
    }

    /**
     * Undocumented function
     *
     * @param StudentRequestIndex $request
     * @param integer $id
     * @return void
     */
    public function StudentsForYear(StudentRequestIndex $request, int $id)
    {
        if ($request->authorize() == true) {
            $students = Student::where('academic_id', $id)->get();
            return $this->sendResponse(StudentResource::collection($students), 'all students sent successfully');
        }
    }
}
