<?php

namespace App\Http\Controllers\API;

use App\Models\Teacher;
use App\Models\User;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Teacher as TeacherResource;
use App\Http\Requests\Api\Teachers\TeacherRequestIndex;
use App\Http\Requests\Api\Teachers\TeacherRequestShow;
use App\Http\Requests\Api\Teachers\TeacherRequestAdd;
use App\Http\Requests\Api\Teachers\TeacherRequestUpdate;

class TeacherController extends BaseController
{

    /**
     * Undocumented function
     *
     * @param TeacherRequestAdd $request
     * @param integer $id
     * @return void
     */
    public function addTeacherInformation(TeacherRequestAdd $request, int $id)
    {
        $user = User::where('id', $id)->whereHas('roles', function ($q) {
            $q->where('id', 2);
        })->first();

        if (is_null($user)) {
            return $this->sendError('teacher not found');
        } else {
            $teacher = new Teacher();
            $teacher->user_id = $id;
            $teacher->birthday = $request->birthday;
            $teacher->phone = $request->phone;
            $teacher->save();
            $teacher->subjects()->attach($request->subject_id);
            /*
            $teacher = Teacher::create([
                'user_id' => $id,
                'birthday' => $request->birthday,
                'phone' => $request->phone,
            ]);*/
        }

        return $this->sendResponse(new TeacherResource($teacher), 'Teacher’s information sent successfully');
    }


    /**
     * Undocumented function
     *
     * @param TeacherRequestIndeax $request
     * @return void
     */
    public function index(TeacherRequestIndex $request)
    {
        if ($request->authorize() == true) {
            $teachers = Teacher::all();
            return $this->sendResponse(TeacherResource::collection($teachers), 'all teachers sent successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param TeacherRequestShow $request
     * @param integer $id
     * @return void
     */
    public function show(TeacherRequestShow $request, int $id)
    {
        if ($request->authorize() == true) {
            $teacher = Teacher::findOrFail($id);
            return $this->sendResponse(new TeacherResource($teacher), 'teacher founded successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param TeacherRequestUpdate $request
     * @param integer $id
     * @return void
     */
    public function update(TeacherRequestUpdate $request, int $id)
    {
        $teacher = Teacher::findOrFail($id);
        $input = $request->all();
        //$teacher->user_id = $input['user_id'];
        $teacher->birthday = $input['birthday'];
        $teacher->phone = $input['phone'];
        $teacher->subjects()->sync($input['subject_id']);
        $teacher->save();

        return $this->sendResponse(new TeacherResource($teacher), 'teacher updated successfully');
    }
}
