<?php

namespace App\Http\Controllers\API;

use App\Models\Subject;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Requests\Api\Subjects\IndexSubjectRequest;
use App\Http\Requests\Api\Subjects\SubjectRequestShow;
use App\Http\Resources\Subject as SubjectResource;

class SubjectController extends BaseController
{
    /**
     * Undocumented function
     *
     * @param IndexSubjectRequest $request
     * @return void
     */
    public function index(IndexSubjectRequest $request)
    {
        if ($request->authorize() == true) {
            $subjects = Subject::all();
            return $this->sendResponse(SubjectResource::collection($subjects), 'all subjects sent successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param SubjectRequestShow $request
     * @param integer $id
     * @return void
     */
    public function show(SubjectRequestShow $request, int $id)
    {
        if ($request->authorize() == true) {
            $subject = Subject::findOrFail($id);
            return $this->sendResponse(new SubjectResource($subject), 'subject founded successfully');
        }
    }


    /**
     * Undocumented function
     *
     * @param IndexSubjectRequest $request
     * @param integer $id
     * @return void
     */
    public function SubjectsForYear(IndexSubjectRequest $request, int $id)
    {
        if ($request->authorize() == true) {
            $subjects = Subject::where('academic_id', $id)->get();
            return $this->sendResponse(SubjectResource::collection($subjects), 'all subjects sent successfully');
        }
    }
}
