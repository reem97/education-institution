<?php

namespace App\Http\Requests\Api\Teachers;

use Illuminate\Foundation\Http\FormRequest;

class TeacherRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAbleTo('teachers-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birthday' => [
                "required"
            ],
            'phone' => [
                "required",
                "digits:10"
            ],

        ];
    }
}
