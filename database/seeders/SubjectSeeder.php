<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject1 = \App\Models\Subject::create([
            'academic_id' => '1',
            'name' => 'programming1',
        ]);

        $subject2 = \App\Models\Subject::create([
            'academic_id' => '1',
            'name' => 'programming2',
        ]);

        $subject3 = \App\Models\Subject::create([
            'academic_id' => '2',
            'name' => 'programming3',
        ]);

        $subject4 = \App\Models\Subject::create([
            'academic_id' => '2',
            'name' => 'algorithms',
        ]);

        $subject5 = \App\Models\Subject::create([
            'academic_id' => '3',
            'name' => 'programming-languages',
        ]);

        $subject6 = \App\Models\Subject::create([
            'academic_id' => '3',
            'name' => 'project1',
        ]);

        $subject7 = \App\Models\Subject::create([
            'academic_id' => '4',
            'name' => 'protocols',
        ]);

        $subject8 = \App\Models\Subject::create([
            'academic_id' => '4',
            'name' => 'project2',
        ]);

        $subject9 = \App\Models\Subject::create([
            'academic_id' => '5',
            'name' => 'distrebuted-system',
        ]);

        $subject9 = \App\Models\Subject::create([
            'academic_id' => '5',
            'name' => 'graduation-project',
        ]);
    }
}
