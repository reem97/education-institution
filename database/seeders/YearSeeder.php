<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class YearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year1 = \App\Models\AcademicYear::create([
            'name' => 'first',
        ]);

        $year2 = \App\Models\AcademicYear::create([
            'name' => 'secound',
        ]);

        $year3 = \App\Models\AcademicYear::create([
            'name' => 'third',
        ]);

        $year4 = \App\Models\AcademicYear::create([
            'name' => 'fourth',
        ]);

        $year5 = \App\Models\AcademicYear::create([
            'name' => 'fifth',
        ]);
    }
}
